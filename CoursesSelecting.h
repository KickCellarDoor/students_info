//
// Created by cxxnzb on 2016/11/14.
//

#ifndef INC_2_COURSESSELECTING_H
#define INC_2_COURSESSELECTING_H
#pragma once

#include <iostream>
#include <string.h>
#include "Student.h"
#include "Courses.h"
using namespace std;

class CoursesSelecting :public Student{
    Courses *course;

public:
    CoursesSelecting();

    CoursesSelecting(Courses *course);

    CoursesSelecting(const string &name, int stu_no, Courses *course);

    Courses* getCourse();

    void setCourse(Courses *course);

    virtual void print() override;
};


#endif //INC_2_COURSESSELECTING_H
