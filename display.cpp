//
// Created by cxxnzb on 16/9/9.
//
#include <cstdlib>
#include "display.h"

void adminlogin(Management_Sys management_sys){
    int num = 3;
    while(num > 0){
        cout << "请输入管理员密码:(剩余机会:" << num << ")" << endl;
        char pwd[16];
        cin >> pwd;
        if(strcmp(pwd,adminpwd) == 0){
            int choice = 0;
            while(choice != 15){
                system("clear");
                admin_menu();
                cin >> choice;
                switch(choice){
                    case 1:{
                        system("clear");
                        cout << "请输入需添加学生信息(学号 姓名):" << endl;
                        char name[64];
                        int id;
                        cin >> id;
                        cin >> name;
                        Student *student = new Student(name,id);
                        if(!management_sys.exists(student))
                            management_sys.add(student);
                        else{
                            cout << "已存在此学生信息!" << endl;
                        }
                        break;
                    }
                    case 2:{
                        system("clear");
                        cout <<"请输入需要删除的学生学号:" << endl;
                        int id;
                        cin >> id;
                        management_sys.del_stu(id);
                        break;
                    }
                    case 3:{
                        system("clear");
                        cout << "请输入需添加课程信息(课程名 学分 课程类型（1 计分 2 通过 3 等级）):" << endl;
                        char name[64];
                        int weight;
                        cin >> name;
                        cin >> weight;
                        int num;
                        cin >> num;
                        Courses *course;

                        switch(num){
                            case 1:{
                                course = new ScoredCourses(name,weight,0);
                                break;
                            }
                            case 2:{
                                course = new PassCourses(name,weight,false);
                                break;
                            }
                            case 3:{
                                course = new GradeCourses(name,weight,'0');
                                break;
                            }
                            default:{
                                exit(0);
                                break;
                            }
                        }
                        if(!management_sys.exists(course)) {
                            management_sys.add(course);
                        } else
                            cout << "已存在此课程" << endl;
                        break;
                    }
                    case 4:{
                        system("clear");
                        cout <<"请输入需要删除的课程名:" << endl;
                        string course;
                        cin >> course;
                        management_sys.del_course(course);
                        break;
                    }
                    case 5:{
                        system("clear");
                        management_sys.sort_by_name();
                        break;
                    }
                    case 6:{
                        system("clear");
                        management_sys.sort_by_id();
                        break;
                    }
                    case 7:{
                        system("clear");
                        management_sys.save_stu_info();
                        break;
                    }
                    case 8:{
                        system("clear");
                        management_sys.save_courses_info();
                        break;
                    }
                    case 9:{
                        system("clear");
                        cout << "请输入学号:" << endl;
                        int num;
                        cin >> num;
                        management_sys.search(num);
                        break;
                    }
                    case 10:{
                        system("clear");
                        cout << "请输入课程名:" << endl;
                        string course;
                        cin >> course;
                        management_sys.search(course);
                        break;
                    }
                    case 11:{
                        system("clear");
                        cout << "请输入学生学号，课程名" << endl;
                        int id;
                        string course;
                        cin >> id;
                        cin >> course;
                        Courses *courses;
                        Courses *temp = management_sys.getCourse(course);
                        if(temp == NULL) {
                            cout << "没有此课程!" << endl;
                            break;
                        }
                        //cout << temp->getType();
                        switch(temp->getType()){
                            case 1:{
                                courses = new ScoredCourses(temp->getName(),temp->getWeight(),0);
                                break;
                            }
                            case 2:{
                                courses = new PassCourses(temp->getName(),temp->getWeight(),false);
                                break;
                            }
                            case 3:{
                                courses = new GradeCourses(temp->getName(),temp->getWeight(),'0');
                                break;
                            }
                            case 0:{
                                courses = new Courses(temp->getName(),temp->getWeight());
                                break;
                            }
                            default:{
                                exit(0);
                                break;
                            }
                        }
                        //cout << management_sys.getStudent(id).getName();
                        CoursesSelecting *coursesSelecting = new CoursesSelecting(management_sys.getStudent(id).getName(),id,courses);
                        management_sys.add(coursesSelecting);
                        break;
                    }
                    case 12:{
                        system("clear");
                        cout << "请输入学号，课程名:" << endl;
                        string course;
                        int id;
                        cin >> id;
                        cin >> course;

                        management_sys.del_selecting(id,course);
                        break;
                    }
                    case 13:{
                        system("clear");
                        cout << "请输入学号，课程名：" << endl;
                        int id;
                        string course;
                        cin >> id;
                        cin >> course;
                        if(management_sys.isSelected(id,course)){
                            switch (management_sys.getCourse(course)->getType()){
                                case 1:{
                                    cout << "请输入分数（百分制）:" << endl;
                                    int score;
                                    cin >> score;
                                    ((ScoredCourses*)(management_sys.getSelecting(id,course)->getCourse()))->setScore(score);
                                    break;
                                }
                                case 2:{
                                    cout << "是否通过(true,false):" << endl;
                                    string is;
                                    cin >> is;
                                    bool j;
                                    if(is == "true")
                                        j = true;
                                    else
                                        j = false;
                                    ((PassCourses*)(management_sys.getSelecting(id,course)->getCourse()))->setPass(j);
                                    break;
                                }
                                case 3:{
                                    cout << "请输入等级:" << endl;
                                    char grade;
                                    cin >> grade;
                                    ((GradeCourses*)(management_sys.getSelecting(id,course)->getCourse()))->setGrade(grade);
                                    break;
                                }
                                case 0:{
                                    cout << "改课程无法设置成绩!" << endl;
                                    break;
                                }
                                default:{
                                    cout << "输入信息不符合要求!" << endl;
                                    break;
                                }
                            }
                        } else{
                            cout << "没有选课记录！" << endl;
                        }
                        break;
                    }
                    case 14:{
                        system("clear");
                        management_sys.showCourses();
                        break;
                    }
                    case 15:{
                        exit(0);
                        break;
                    }
                    default:{
                        if(choice != 15)
                            cout << "请再次输入:" << endl;
                        break;
                    }
                }
                pause();
            }
            return;
        }
        else {
            num--;
        }
    }
    cout << "密码输入此处达到上限,拒绝访问!"<< endl;
    return;
}

void admin_menu(){
    cout << "--------------------------------------------------------------------------------------------------------------" << endl;
    cout << "--                                    1.添加学生信息                                                          " << endl;
    cout << "--                                    2.删除学生信息                                                          " << endl;
    cout << "--                                    3.添加课程信息                                                          " << endl;
    cout << "--                                    4.删除课程信息                                                          " << endl;
    cout << "--                                    5.按姓名排序                                                            " << endl;
    cout << "--                                    6.按学号排序                                                            " << endl;
    cout << "--                                    7.确认更改并保存学生信息至文件                                             " << endl;
    cout << "--                                    8.确认更改并保存课程信息至文件                                             " << endl;
    cout << "--                                    9.查看指定学号学生选课信息                                                " << endl;
    cout << "--                                    10.查看指定课程选课信息                                                   " << endl;
    cout << "--                                    11.添加选课信息                                                          " << endl;
    cout << "--                                    12.删除选课信息                                                          " << endl;
    cout << "--                                    13.设置分数                                                             " << endl;
    cout << "--                                    14.显示课程                                                             " << endl;
    cout << "--                                    15.退出                                                               " << endl;
    cout << "--------------------------------------------------------------------------------------------------------------" << endl;
    cout << "请输入: ";
}

void pause(){
    cout << "输入任意值以返回:" << endl;
    char temp[128];
    cin >> temp;
}