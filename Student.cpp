//
// Created by cxxnzb on 2016/11/14.
//

#include "Student.h"

const string &Student::getName() const {
    return name;
}

void Student::setName(const string &name) {
    Student::name = name;
}

int Student::getStu_no() const {
    return stu_no;
}

void Student::setStu_no(int stu_no) {
    Student::stu_no = stu_no;
}

Student::Student(const string &name, int stu_no) : name(name), stu_no(stu_no) {}

Student::Student() {}

void Student::print() {
    cout << stu_no << "\t" << name;
}

