//
// Created by cxxnzb on 2016/11/14.
//

#include "Courses.h"

Courses::Courses() {}

Courses::Courses(const string &name, int weight) : name(name), weight(weight) {
    type = 0;
    set_grade = false;
}

const string &Courses::getName() const {
    return name;
}

void Courses::setName(const string &name) {
    Courses::name = name;
}

int Courses::getWeight() const {
    return weight;
}

void Courses::setWeight(int weight) {
    Courses::weight = weight;
}

void Courses::print() const{
    cout << name << "\t" << weight << "\t";
}

bool Courses::operator==(const Courses &rhs) const {
    return name == rhs.name &&
           weight == rhs.weight;
}

bool Courses::operator!=(const Courses &rhs) const {
    return !(rhs == *this);
}

int Courses::getType() const {
    return type;
}

void Courses::setType(int type) {
    Courses::type = type;
}
