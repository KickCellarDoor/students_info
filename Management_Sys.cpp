//
// Created by cxxnzb on 2016/11/14.
//

#include "Management_Sys.h"

void Management_Sys::init() {
    string stu_address = "/Users/cxxnzb/Desktop/stu.txt";
    string courses_address = "/Users/cxxnzb/Desktop/courses.txt";

    ifstream stu_file(stu_address);
    ifstream courses_file(courses_address);

    if(!stu_file.is_open()){
        cout << "Can't open the file!";
        exit(1);
    } else{
        char buffer[256];
        while(!stu_file.eof()){
            stu_file.getline(buffer,256);
            if(buffer[0] == '\0')
                continue;
            stringstream stringstream1(buffer);
            string name;
            int no;
            stringstream1 >> no;
            stringstream1 >> name;

            Student *student = new Student(name,no);

            students.push_back(student);
        }
    }
    stu_file.close();

    if(!courses_file.is_open()){
        cout << "Can't open the file!";
        exit(1);
    } else{
        char buffer[256];
        while(!courses_file.eof()){
            courses_file.getline(buffer,256);
            if(buffer[0] == '\0')
                continue;
            stringstream stringstream1(buffer);
            string name;
            int weight;
            int type;
            stringstream1 >> name;
            stringstream1 >> weight;
            stringstream1 >> type;
            Courses *courses1;
            switch(type){
                case 1: {
                    courses1 = new ScoredCourses(name, weight, 0);
                    break;
                }
                case 2: {
                    courses1 = new PassCourses(name, weight, false);
                    break;
                }
                case 3: {
                    courses1 = new GradeCourses(name, weight, '0');
                    break;
                }
            }

            courses.push_back(courses1);
        }
    }

    courses_file.close();
}

void Management_Sys::add(Student *student) {
    students.push_back(student);
}

void Management_Sys::add(Courses *course) {
    courses.push_back(course);
}

void Management_Sys::add(CoursesSelecting *coursesSelecting) {
    if(!exists(coursesSelecting))
        courses_selecting.push_back(coursesSelecting);
    else
        cout << "已经存在此选课信息" << endl;
}

void Management_Sys::del_course(string course) {
    for(auto i = courses.begin();i != courses.end();i++){
        if((*i)->getName() == course)
            courses.erase(i++);
    }

    del_by_course(course);
}

void Management_Sys::del_selecting(int stu_no, string course) {
    for(auto i = courses_selecting.begin();i != courses_selecting.end();i++){
        if((*i)->getStu_no() == stu_no && (*i)->getCourse()->getName() == course){
            courses_selecting.erase(i++);
        }
    }
}

void Management_Sys::del_stu(int no) {
    for(auto i = students.begin();i != students.end();i++){
        if((*i)->getStu_no() == no)
            students.erase(i++);
    }
    del_by_id(no);
}

void Management_Sys::save_courses_info() {
    string courses_address = "/Users/cxxnzb/Desktop/courses.txt";

    ofstream out(courses_address);

    for(auto a : courses){
        out << "\n";
        out << a->getName();
        out << "\t";
        out << a->getWeight();
        out << "\t";
        out << a->getType();
    }

    out.close();
}

void Management_Sys::save_stu_info() {
    string stu_address = "/Users/cxxnzb/Desktop/stu.txt";
    ofstream out(stu_address);

    for(auto a : students){
        out << "\n";
        out << a->getStu_no();
        out << "\t";
        out << a->getName();
    }

    out.close();
}

void Management_Sys::select(int stu_no,string name, Courses course) {
    if(isSelected(stu_no,course.getName())){
        cout << "选课信息已经存在" << endl;
        return;
    }
    CoursesSelecting *coursesSelecting = new CoursesSelecting(name,stu_no,&course);
    courses_selecting.push_back(coursesSelecting);
}

void Management_Sys::undo_select(int stu_no, Courses course) {
    for(auto i = courses_selecting.begin();i != courses_selecting.end();i++){
        if((*i)->getStu_no() == stu_no && *((*i)->getCourse()) == course){
            courses_selecting.erase(i++);
            return;
        }
    }
    cout << "没有此选课信息!" << endl;
}

void Management_Sys::search(int stu_no) {
    for(auto a : courses_selecting){
        if(a->getStu_no() == stu_no) {
          a->print();
        }
    }
}

void Management_Sys::search(string course) {
    for(auto a : courses_selecting){
        if(a->getCourse()->getName() == course){
            a->print();
        }
    }
}

void Management_Sys::del_by_course(string course) {
    for(auto i = courses_selecting.begin();i != courses_selecting.end();i++){
        if((*i)->getCourse()->getName() == course) {
            courses_selecting.erase(i++);
        }
    }
}

void Management_Sys::del_by_id(int id) {
    for(auto i = courses_selecting.begin();i != courses_selecting.end();i++){
        if((*i)->getStu_no() == id) {
            courses_selecting.erase(i++);
        }
    }
}

bool comp1(Student *student1,Student *student2){
    return student1->getStu_no() < student2->getStu_no();
}

bool comp2(Student *student1,Student *student2){
    return student1->getName() < student2->getName();
}

void Management_Sys::sort_by_id() {
    students.sort(comp1);
    for(auto a : students) {
        a->print();
        cout << endl;
    }
}

void Management_Sys::sort_by_name() {
    students.sort(comp2);

    for(auto a : students){
        a->print();
        cout << endl;
    }
}

Courses* Management_Sys::getCourse(string course) {
    for(auto a : courses){
        if(a->getName() == course)
            return a;
    }
    return NULL;
}

Student Management_Sys::getStudent(int id) {
    for(auto a : students){
        if(a->getStu_no() == id)
            return *a;
    }
    Student student;
    return student;
}

bool Management_Sys::isSelected(int stu_no, string course) {
    for(auto a : courses_selecting){
        if(a->getStu_no() == stu_no && a->getCourse()->getName() == course)
            return true;
    }
    return false;
}

CoursesSelecting *Management_Sys::getSelecting(int stu_no, string course) {
    for(auto a : courses_selecting){
        if(a->getStu_no() == stu_no && a->getCourse()->getName() == course)
            return a;
    }
    return NULL;
}

bool Management_Sys::exists(Student *student) {
    for(auto a : students){
        if(a->getStu_no() == student->getStu_no())
            return true;
    }
    return false;
}

bool Management_Sys::exists(Courses *course) {
    for(auto a : courses){
        if(a->getName() == course->getName())
            return true;
    }
    return false;
}

bool Management_Sys::exists(CoursesSelecting *coursesSelecting) {
    for(auto a : courses_selecting){
        if(coursesSelecting->getStu_no() == a->getStu_no() && coursesSelecting->getCourse()->getName() == a->getCourse()->getName())
            return true;
    }
    return false;
}

void Management_Sys::showCourses() {
    for(auto a : courses){
        cout << a->getName() << "\t" << a->getWeight() << "\t";
        switch (a->getType()){
            case 1:cout << "计分" << endl;
                break;
            case 2:cout << "通过" << endl;
                break;
            case 3:cout << "等级" << endl;
                break;
        }
    }
}