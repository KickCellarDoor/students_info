//
// Created by cxxnzb on 2016/11/14.
//

#ifndef INC_2_STUDENT_H
#define INC_2_STUDENT_H

#pragma once
#include <iostream>
#include <string.h>

using namespace std;
class Student {
    string name;
    int stu_no;
public:
    Student();

    Student(const string &name, int stu_no);

    const string &getName() const;

    void setName(const string &name);

    int getStu_no() const;

    void setStu_no(int stu_no);

    virtual void print();

};


#endif //INC_2_STUDENT_H
