//
// Created by cxxnzb on 2016/11/14.
//

#include "CoursesSelecting.h"

CoursesSelecting::CoursesSelecting(Courses *course) {
    this->course = course;
}

CoursesSelecting::CoursesSelecting(const string &name, int stu_no, Courses *course) : Student(name, stu_no){
    this->course = course;
}

Courses* CoursesSelecting::getCourse() {
    return course;
}

void CoursesSelecting::setCourse(Courses *course) {
    CoursesSelecting::course = course;
}

void CoursesSelecting::print() {
    Student::print();
    cout << "\t";
    course->print();
    cout << endl;
}

CoursesSelecting::CoursesSelecting() {}
