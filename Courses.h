//
// Created by cxxnzb on 2016/11/14.
//

#ifndef INC_2_COURSES_H
#define INC_2_COURSES_H
#pragma once

#include <iostream>
#include <string.h>
using namespace std;

class Courses {
    string name;
    int weight;
protected:
    bool set_grade;
    int type;
public:
    Courses();

    Courses(const string &name, int weight);

    const string &getName() const;

    void setName(const string &name);

    int getWeight() const;

    void setWeight(int weight);

    virtual void print() const;

    bool operator==(const Courses &rhs) const;

    bool operator!=(const Courses &rhs) const;

    int getType() const;

    void setType(int type);

};

class PassCourses : public Courses{
    bool isPass;
public:
    PassCourses(const string &name, int weight, bool isPass) : Courses(name, weight), isPass(isPass) {
        type = 2;
    }

    void setPass(bool isPass){
        this->isPass = isPass;
        set_grade = true;
    }
    bool getIsPass(){
        return isPass;
    }

    virtual void print() const override {
        Courses::print();
        if(set_grade) {
            if (isPass == true) {
                cout << "\t" << "PASSED" << endl;
            } else {
                cout << "\t" << "Can't pass" << endl;
            }
        } else{
            cout << "\t" << "No grade now" << endl;
        }
    }
};

class GradeCourses : public Courses{
    char grade;
public:
    GradeCourses(const string &name, int weight, char grade) : Courses(name, weight), grade(grade) {
        type = 3;
    }

    void setGrade(char grade){
        this->grade = grade;
        set_grade = true;
    }

    char getGrade(){
        return grade;
    }

    virtual void print() const override {
        Courses::print();
        if(set_grade)
            cout << "\t" << grade << endl;
        else
            cout << "\t" << "No grade now" << endl;
    }
};

class ScoredCourses : public Courses{
    int score;
public:
    ScoredCourses(const string &name, int weight, int score) : Courses(name, weight), score(score) {
        type = 1;
    }

    void setScore(int score){
        this->score = score;
        set_grade = true;
    }

    int getScore(){
        return score;
    }

    virtual void print() const override {
        Courses::print();
        if(set_grade)
            cout << "\t" << score << endl;
        else
            cout << "\t" << "No grade now" << endl;
    }
};


#endif //INC_2_COURSES_H
