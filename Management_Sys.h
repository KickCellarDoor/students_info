//
// Created by cxxnzb on 2016/11/14.
//

#ifndef INC_2_MANAGEMENT_SYS_H
#define INC_2_MANAGEMENT_SYS_H
#pragma once

#include "CoursesSelecting.h"
#include <algorithm>
#include <list>
#include <fstream>
#include <sstream>
using namespace std;

class Management_Sys {
    list<Student*> students;
    list<Courses*> courses;
    list<CoursesSelecting*> courses_selecting;

public:
    Management_Sys(){}

    void init();

    void add(Student *student);

    void add(Courses *course);

    void add(CoursesSelecting *coursesSelecting);

    void del_stu(int no);

    void del_course(string course);

    void del_selecting(int stu_no,string course);

    void save_stu_info();

    void save_courses_info();

    void select(int stu_no,string name,Courses course);

    void undo_select(int stu_no,Courses course);

    void search(int stu_no);

    void search(string course);

    void del_by_id(int id);

    void del_by_course(string course);

    void sort_by_id();

    void sort_by_name();

    Courses *getCourse(string course);

    Student getStudent(int id);

    bool isSelected(int stu_no,string course);

    CoursesSelecting *getSelecting(int stu_no,string course);

    bool exists(Student *student);

    bool exists(Courses *course);

    bool exists(CoursesSelecting *coursesSelecting);

    void showCourses();
};


#endif //INC_2_MANAGEMENT_SYS_H
